const books = [
  { title: "The Da Vinci Code", author: "Dan Brown", genre: "Thriller" },
  { title: "Inferno", author: "Dan Brown", genre: "Thriller" },
  {
    title: "Programming for Dummies",
    author: "Dan Gookin",
    genre: "Technology",
  },
];

const danBrown = {
  name: "Dan Brown",
  dateOfBirth: Date(1955, 10, 10),
  country: "USA",
};

function getBooks(callback) {
  const ms = Math.random() * 1000;
  setTimeout(function () {
    callback(null, books);
  }, ms);
}

function getAuthorDetails(author, callback) {
  const ms = Math.random() * 1000;
  let result;
  if (author === danBrown.name) {
    result = danBrown;
  }

  setTimeout(function () {
    if (result) {
      callback(null, result);
    } else {
      callback("Author not found!");
    }
  }, ms);
}

exports.getBooks = getBooks;
exports.getAuthorDetails = getAuthorDetails;
