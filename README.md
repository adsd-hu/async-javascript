# async-javascript

Examples of calling asynchronous code in Javascript. Three possible solutions, see branches `callbacks`, `promises` and `async/await`.
